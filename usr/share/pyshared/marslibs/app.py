import argparse
from sys import argv
from os import getcwd
from os.path import expanduser

def getOptions():
    '''
    Analizu datumoj
    '''
    args = argv[1:]
    parser = argparse.ArgumentParser(description="Parses command.",argument_default=argparse.SUPPRESS)
    #parser.add_argument("echo",nargs="?")
    parser.add_argument("-c", "--config",nargs="?",
                        help="Optional path to a non-default config file.")
    #parser.add_argument("-s", "--shell",dest='shell',action='store_true',help="Keep it in the shell. Do not launch the GUI.")
    #parser.add_argument("-g", "--gui",dest='gui',action='store_true',help="Run the GUI.")
    #parser.add_argument("-q", "--quiet",dest='quiet',action='store_true',help="Do not notify me when finished.")

    options = parser.parse_args(args)
    opts = vars(options)

    '''not in use right now
    if not opts.has_key('config'):
        try:
            opts['config'] = '/etc/frizzle/frizzle.conf'
        except:
            print('Failed: No template file found.')
            exit()
    '''

    if not opts.has_key('shell'):
        try:
            from PySide.QtGui import QApplication
        except ImportError:
            try:
                from PyQt4.QtGui import QApplication
            except:
                import shell
                shell.execute(opts)
        import gui
        gui.execute(opts)

    else:
        import shell
        shell.execute(opts)
