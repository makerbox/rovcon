import sys
import time
import argparse
import serial
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class Fenestro(QMainWindow):
    def __init__(self,opts,parent=None):
        super(Fenestro, self).__init__(parent)
        self.central = QWidget()
        #self.fname = opts['echo']
        self.setCentralWidget(self.central)
        self.setWindowIcon(QIcon('/usr/share/icons/oxygen/64x64/devices/input-gaming.png'))
        '''
        cssfile="etc/frizzle/style.css"

        with open(cssfile,"r") as css:
            self.setStyleSheet(css.read())
        '''

        self.setWindowTitle("Mars")

        #fields
        self.appname = QLabel("ROVCON")
        appfont = QFont('Sans', 48, QFont.Bold)
        self.appname.setFont(appfont)
        self.forward = QPushButton()
        self.forward.setFlat(True)
        self.forward.setAutoRepeat(True)
        self.forward.setShortcut('w')
        self.forward.clicked.connect(self.go_forward)
        self.forward.setIcon(QIcon('/usr/share/icons/oxygen/64x64/actions/go-up.png'))
        self.forward.setIconSize(QSize(64,64))

        self.reverse = QPushButton()
        self.reverse.setFlat(True)
        self.reverse.setAutoRepeat(True)
        self.reverse.setShortcut('s')
        self.reverse.clicked.connect(self.go_reverse)
        self.reverse.setIcon(QIcon('/usr/share/icons/oxygen/64x64/actions/go-down.png'))
        self.reverse.setIconSize(QSize(64,64))

        self.right = QPushButton()
        self.right.setFlat(True)
        self.right.setAutoRepeat(True)
        self.right.setShortcut('d')
        self.right.clicked.connect(self.go_right)
        self.right.setIcon(QIcon('/usr/share/icons/oxygen/64x64/actions/go-next.png'))
        self.right.setIconSize(QSize(64,64))

        self.left = QPushButton()
        self.left.setFlat(True)
        self.left.setAutoRepeat(True)
        self.left.setShortcut('a')
        self.left.clicked.connect(self.go_left)
        self.left.setIcon(QIcon('/usr/share/icons/oxygen/64x64/actions/go-previous.png'))
        self.left.setIconSize(QSize(64,64))

        self.light = QPushButton()
        self.light.clicked.connect(self.go_light)
        self.light.setIcon(QIcon('/usr/share/icons/oxygen/64x64/status/user-offline.png'))
        self.light.setIconSize(QSize(64,64))
        self.light.setCheckable(1)

        self.conblue = QPushButton()
        self.conblue.clicked.connect(self.go_conblue)
        self.conblue.setIcon(QIcon('/usr/share/icons/oxygen/64x64/devices/network-wireless-connected-25.png'))
        self.conblue.setIconSize(QSize(48,48))
        self.conblue.setCheckable(1)
        self.conblue.setToolTip('Connect')
        
        #layout
        gridbox = QVBoxLayout(self.central)
        bbox = QGridLayout()
        titlebox = QHBoxLayout()

        gridbox.addLayout(titlebox)
        gridbox.addLayout(bbox)

        titlebox.addWidget(self.appname)
        titlebox.addWidget(self.conblue)

        bbox.addWidget(self.forward, 1,1)
        bbox.addWidget(self.left, 2,0)
        bbox.addWidget(self.light, 2,1)
        bbox.addWidget(self.right, 2,2)
        bbox.addWidget(self.reverse, 3,1)

        self.setGeometry(200,200,450,250)

        self.statusBar().showMessage('Ready')
        #create toolbar
        exitAction = QAction(QIcon('/usr/share/icons/oxygen/22x22/actions/application-exit.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(qApp.quit)        
        self.toolbar = self.addToolBar('Exit')
        self.toolbar.addAction(exitAction)
        self.statusBar().showMessage('Status: Disconnected')
        # init finished, launch main loop
        self.Main()

    def Main(self):
        """Main Loop
        This is the main loop. It sits patiently
        and listens for commands, like button presses.
        """
        print('main loop..')

    def sendcomm(self, action):
        if self.ser:
            for val in action:
                print(val)
                self.ser.write(bytes(val))
                #self.ser.write(val)
            self.ser.flush()

    def go_stop(self):
        self.sendcomm('S')

    def go_forward(self):
        if self.forward.isDown():
            self.sendcomm('F')
        else:
            self.sendcomm('S')

    def go_reverse(self):
        if self.reverse.isDown():
            self.sendcomm('B')
        else:
            self.sendcomm('S')

    def go_right(self):
        if self.right.isDown():
            self.sendcomm('R')
        else:
            self.sendcomm('S')

    def go_left(self):
        if self.left.isDown():
            self.sendcomm('L')
        else:
            self.sendcomm('S')

    def go_light(self):
        if self.light.isChecked():
            print('lights on') #debug
            self.sendcomm('W')
            self.light.setIcon(QIcon('/usr/share/icons/oxygen/64x64/status/user-online.png'))
        else:
            print('lights off')
            self.sendcomm('w')
            self.light.setIcon(QIcon('/usr/share/icons/oxygen/64x64/status/user-offline.png'))

    def go_conblue(self):
        if self.conblue.isChecked():
            # make the conection
            try:
                self.ser = serial.Serial('/dev/rfcomm0', 9600)
            except IOError:
                self.statusBar().showMessage('Error attempting to connect.')
                print("Error attempting to connect.")

            # tell the user
            print('connecting') #debug
            self.conblue.setToolTip('Connect')
            self.conblue.setIcon(QIcon('/usr/share/icons/oxygen/64x64/devices/network-wireless-connected-100.png'))
            self.appname.setStyleSheet('QLabel {color: #0099CC; }')
            self.statusBar().showMessage('Status: Connected')
        else:

            # make the DISconnectiong
            try:
                self.sendcomm('c')
                time.sleep(2)
                self.ser.close()
            except IOError:
                print("Error closing port.")

            print('disconnect sig')
            self.conblue.setToolTip('Disconnect')
            self.statusBar().showMessage('Status: Disconnected')
            self.appname.setStyleSheet('QLabel {color: #333; }')
            self.conblue.setIcon(QIcon('/usr/share/icons/oxygen/64x64/devices/network-wireless-connected-25.png'))



def execute(opts):
    marsgui = QApplication(sys.argv)
    window = Fenestro(opts)
    print('App launched.')
    window.show()
    marsgui.exec_()
