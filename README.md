ROVCON
======

Simple Python Qt4 GUI to drive a remote control rover made with an
Arduino and HC-05 bluetooth module created for a class taught by
[MakerBox](http://makerbox.org.nz).


Requirements (Software)
-----------------------

- [Python](http://python.org)

- [Qt4](http://qt.io)

- [PyQt4](http://pyqt.sourceforge.net) or PySide



Requirements (Hardware)
-----------------------

- Arduino (we used an Uno)

- HC-05 Bluetooth module (for the Arduino)

- Bluetooth (in your computer, or as a dongle)



Setup
-----

On your computer, you must:

- Assign the HC-05 bluetooth module to the serial port of your
computer.

- Give yourself permission to use the serial port of your
computer.


To do this:

1. Get the MAC address of the HC-05. The MAC address of my HC-05 is 20:15:05:21:17:39 so I would do this:

   <pre>
   $ sudo mkdir /var/lib/bluetooth/20\:15\:05\:21\:17\:39
   </pre>

2. Then set the pin; on the HC-05 the default is 1234.

   <pre>
   $ sudo echo "1234" > /var/lib/bluetooth/20\:15\:05\:21\:17\:39/pincodes
   </pre>

3. Next, marry your computer's serial port to the HC-05. This is done
   in the file rfcomm.conf, found in `/etc/bluetooth/rfcomm.conf`. The
   values are pretty intuitive. You can do this for any number of
   serial connections, so we will start, logically, with `rfcomm0`:

   <pre>
   # RFCOMM configuration file.
   rfcomm0 {
        # Automatically bind the device at startup
        bind yes;
        device 20:15:05:21:19:32;
        # RFCOMM channel for the connection
        channel 1;
        comment "HC-05"; 
   }
   </pre>

4. Finally, add yourself to the `dialout` group so you can interact freely with the serial port:

   <pre>
   $ sudo usermod -a dialout $USER
   </pre>

   Log out and then log back in, and you're ready to use this controller.


Sorry, I don't know how to set up serial ports on OS X or Windows.


Arduino Note
-------------

You need to programme your Arduino to *respond* to the commands this
application sends it, otherwise this application will do you no
good. Arduino code is not included here; this is just a front end for
controlling it.

We used some code we found pretty randomly online; you may have to go
through the code in this application and adjust what key presses the
app sends (or go through the Arduino code and adjust what key presses
it listens for).

Also, you have to wire the Arduino according to whatever programme
you are running on it. 

If we start using this more often in classes, we will probably end up
formalising the Arduino code, which we would then post here.

